<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57"          href="{{url('fav/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60"          href="{{url('fav/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72"          href="{{url('fav/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76"          href="{{url('fav/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114"        href="{{url('fav/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120"        href="{{url('fav/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144"        href="{{url('fav/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152"        href="{{url('fav/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180"        href="{{url('fav/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"   href="{{url('fav/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32"     href="{{url('fav/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96"     href="{{url('fav/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16"     href="{{url('fav/favicon-16x16.png')}}">
    <link rel="manifest" href="/manifest.json">
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <script src="https://www.google.com/recaptcha/api.js?render=_reCAPTCHA_site_key"></script>

    <title>Thai Baht Digital</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">


    <style>
        tr{
            height: 50px;
        }

        .form-control{
            width: 100%
        }
        .nav-link{
            color:#111111;
        }

        #all{
            height: calc( 100vh);
            width: 100vw;
            margin-top: 25px;
            font-size: 21px;
            text-align: center;
            animation: fadein 2s;
            -moz-animation: fadein 2s; /* Firefox */
            -webkit-animation: fadein 2s; /* Safari and Chrome */
            -o-animation: fadein 2s; /* Opera */
        }
        @keyframes fadein {
            from {
                background-color: gray;
            }
            to {
                background-color: white;
            }
        }
        @-moz-keyframes fadein { /* Firefox */
            from {
                background-color: gray;
            }
            to {
                background-color: white;
            }
        }
        @-webkit-keyframes fadein { /* Safari and Chrome */
            from {
                background-color: gray;
            }
            to {
                background-color: white;
            }
        }
        @-o-keyframes fadein { /* Opera */
            from {
                background-color: gray;
            }
            to {
                background-color: white;
            }
        }
    </style>


</head>
<body>
<div class="container-fluid" id="all" style="margin-top: 0px;">
    <div class="sticky"  style="z-index: 999;">
        <nav class="navbar navbar-expand-sm">
            <a class="nav-link" href="{{url('')}}">
                <span>Home</span>
            </a>
        </nav>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <div> @foreach($errors->all() as $error)
                    <p>{{$error}}</p>
                @endforeach
            </div>
        </div>
    @endif
{{--    @if (\Session::has('success'))--}}
{{--        <div class="alert alert-success">--}}
{{--            <p>{{ \Session::get('success') }}</p>--}}
{{--        </div>--}}
{{--    @endif--}}
    <div class="container-fluid">
        <div class="container">
            <div class="row" style="height: 20px;"></div>
            <div class="row" style="text-align: center">
                <div class="col-12" style="padding-bottom: 0px">

                </div>
            </div>
            <div class="row" >
                <div class="col-sm-12 " style="text-align: center" >
                    <img style="height:80px; width: 80px;" src="{{url('img/TBD_Logo.png')}}">
                    <h5 class="card-title" style="padding-top: 15px;">Create a Thai Baht Digital Account</h5>
                </div>

                <div class="d-none d-sm-block col-sm-3 " ></div>

                <div class="col-sm-12 col-md-6" >
                    <div class="card-body">
                        <form  id="regis_form" method="post" name="registration" action="{{url('register')}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="form_email">Email address</label>
                                <input type="email" class="form-control" id="form_email" name="email_address" placeholder="name@example.com">
                                <span class="error_form" style="color: red;font-size: 15px" id="email_error_message"></span>
                            </div>
                            <div class="form-group">
                                <label for="form_password">Password</label>
                                <input type="Password" class="form-control" id="form_password" name="password" placeholder="Password">
                                <span class="error_form" style="color: red;font-size: 15px" id="password_error_message"></span>
                            </div>
                            <div class="form-group">
                                <label for="form_password_confirmation">Confirm Password</label>
                                <input type="Password" class="form-control" id="form_password_confirmation" name="password_confirmation" placeholder="Confirm Password">
                                <span class="error_form" style="color: red;font-size: 15px" id="password_confirmation_error_message"></span>
                            </div>
                            <div class="form-group">
                                <label for="form_firstname">Firstname</label>
                                <input type="text" class="form-control" id="form_firstname" name="firstname" placeholder="">
                                <span class="error_form" style="color: red;font-size: 15px" id="firstname_error_message"></span>
                            </div>

                            <div class="form-group">
                                <label for="form_lastname">Lastname</label>
                                <input type="text" class="form-control" id="form_lastname" name="lastname" placeholder="">
                                <span class="error_form" style="color: red;font-size: 15px" id="lastname_error_message"></span>
                            </div>

                            <div class="form-group">
                                <label for="form_phone_number">Phone number</label>
                                <input type="text" class="form-control" id="form_phone_number" name="phonenumber"  placeholder="">
                                <span class="error_form" style="color: red;font-size: 15px" id="phone_number_error_message"></span>
                            </div>

                            <div class="form-group">
                                <label for="form_province">Province/State</label>
                                <span class="error_form" id="province_error_message"></span>
                                <input type="text" class="form-control" id="form_province" name="province"  placeholder="">
                            </div>

                            <div class="form-group">
                                <label for="Country">Country</label>
                                <select class="form-control" id="Country" name="country">
                                    <option value="AF">Afghanistan</option>
                                    <option value="AX">Åland Islands</option>
                                    <option value="AL">Albania</option>
                                    <option value="DZ">Algeria</option>
                                    <option value="AS">American Samoa</option>
                                    <option value="AD">Andorra</option>
                                    <option value="AO">Angola</option>
                                    <option value="AI">Anguilla</option>
                                    <option value="AQ">Antarctica</option>
                                    <option value="AG">Antigua and Barbuda</option>
                                    <option value="AR">Argentina</option>
                                    <option value="AM">Armenia</option>
                                    <option value="AW">Aruba</option>
                                    <option value="AU">Australia</option>
                                    <option value="AT">Austria</option>
                                    <option value="AZ">Azerbaijan</option>
                                    <option value="BS">Bahamas</option>
                                    <option value="BH">Bahrain</option>
                                    <option value="BD">Bangladesh</option>
                                    <option value="BB">Barbados</option>
                                    <option value="BY">Belarus</option>
                                    <option value="BE">Belgium</option>
                                    <option value="BZ">Belize</option>
                                    <option value="BJ">Benin</option>
                                    <option value="BM">Bermuda</option>
                                    <option value="BT">Bhutan</option>
                                    <option value="BO">Bolivia, Plurinational State of</option>
                                    <option value="BQ">Bonaire, Sint Eustatius and Saba</option>
                                    <option value="BA">Bosnia and Herzegovina</option>
                                    <option value="BW">Botswana</option>
                                    <option value="BV">Bouvet Island</option>
                                    <option value="BR">Brazil</option>
                                    <option value="IO">British Indian Ocean Territory</option>
                                    <option value="BN">Brunei Darussalam</option>
                                    <option value="BG">Bulgaria</option>
                                    <option value="BF">Burkina Faso</option>
                                    <option value="BI">Burundi</option>
                                    <option value="KH">Cambodia</option>
                                    <option value="CM">Cameroon</option>
                                    <option value="CA">Canada</option>
                                    <option value="CV">Cape Verde</option>
                                    <option value="KY">Cayman Islands</option>
                                    <option value="CF">Central African Republic</option>
                                    <option value="TD">Chad</option>
                                    <option value="CL">Chile</option>
                                    <option value="CN">China</option>
                                    <option value="CX">Christmas Island</option>
                                    <option value="CC">Cocos (Keeling) Islands</option>
                                    <option value="CO">Colombia</option>
                                    <option value="KM">Comoros</option>
                                    <option value="CG">Congo</option>
                                    <option value="CD">Congo, the Democratic Republic of the</option>
                                    <option value="CK">Cook Islands</option>
                                    <option value="CR">Costa Rica</option>
                                    <option value="CI">Côte d'Ivoire</option>
                                    <option value="HR">Croatia</option>
                                    <option value="CU">Cuba</option>
                                    <option value="CW">Curaçao</option>
                                    <option value="CY">Cyprus</option>
                                    <option value="CZ">Czech Republic</option>
                                    <option value="DK">Denmark</option>
                                    <option value="DJ">Djibouti</option>
                                    <option value="DM">Dominica</option>
                                    <option value="DO">Dominican Republic</option>
                                    <option value="EC">Ecuador</option>
                                    <option value="EG">Egypt</option>
                                    <option value="SV">El Salvador</option>
                                    <option value="GQ">Equatorial Guinea</option>
                                    <option value="ER">Eritrea</option>
                                    <option value="EE">Estonia</option>
                                    <option value="ET">Ethiopia</option>
                                    <option value="FK">Falkland Islands (Malvinas)</option>
                                    <option value="FO">Faroe Islands</option>
                                    <option value="FJ">Fiji</option>
                                    <option value="FI">Finland</option>
                                    <option value="FR">France</option>
                                    <option value="GF">French Guiana</option>
                                    <option value="PF">French Polynesia</option>
                                    <option value="TF">French Southern Territories</option>
                                    <option value="GA">Gabon</option>
                                    <option value="GM">Gambia</option>
                                    <option value="GE">Georgia</option>
                                    <option value="DE">Germany</option>
                                    <option value="GH">Ghana</option>
                                    <option value="GI">Gibraltar</option>
                                    <option value="GR">Greece</option>
                                    <option value="GL">Greenland</option>
                                    <option value="GD">Grenada</option>
                                    <option value="GP">Guadeloupe</option>
                                    <option value="GU">Guam</option>
                                    <option value="GT">Guatemala</option>
                                    <option value="GG">Guernsey</option>
                                    <option value="GN">Guinea</option>
                                    <option value="GW">Guinea-Bissau</option>
                                    <option value="GY">Guyana</option>
                                    <option value="HT">Haiti</option>
                                    <option value="HM">Heard Island and McDonald Islands</option>
                                    <option value="VA">Holy See (Vatican City State)</option>
                                    <option value="HN">Honduras</option>
                                    <option value="HK">Hong Kong</option>
                                    <option value="HU">Hungary</option>
                                    <option value="IS">Iceland</option>
                                    <option value="IN">India</option>
                                    <option value="ID">Indonesia</option>
                                    <option value="IR">Iran, Islamic Republic of</option>
                                    <option value="IQ">Iraq</option>
                                    <option value="IE">Ireland</option>
                                    <option value="IM">Isle of Man</option>
                                    <option value="IL">Israel</option>
                                    <option value="IT">Italy</option>
                                    <option value="JM">Jamaica</option>
                                    <option value="JP">Japan</option>
                                    <option value="JE">Jersey</option>
                                    <option value="JO">Jordan</option>
                                    <option value="KZ">Kazakhstan</option>
                                    <option value="KE">Kenya</option>
                                    <option value="KI">Kiribati</option>
                                    <option value="KP">Korea, Democratic People's Republic of</option>
                                    <option value="KR">Korea, Republic of</option>
                                    <option value="KW">Kuwait</option>
                                    <option value="KG">Kyrgyzstan</option>
                                    <option value="LA">Lao People's Democratic Republic</option>
                                    <option value="LV">Latvia</option>
                                    <option value="LB">Lebanon</option>
                                    <option value="LS">Lesotho</option>
                                    <option value="LR">Liberia</option>
                                    <option value="LY">Libya</option>
                                    <option value="LI">Liechtenstein</option>
                                    <option value="LT">Lithuania</option>
                                    <option value="LU">Luxembourg</option>
                                    <option value="MO">Macao</option>
                                    <option value="MK">Macedonia, the former Yugoslav Republic of</option>
                                    <option value="MG">Madagascar</option>
                                    <option value="MW">Malawi</option>
                                    <option value="MY">Malaysia</option>
                                    <option value="MV">Maldives</option>
                                    <option value="ML">Mali</option>
                                    <option value="MT">Malta</option>
                                    <option value="MH">Marshall Islands</option>
                                    <option value="MQ">Martinique</option>
                                    <option value="MR">Mauritania</option>
                                    <option value="MU">Mauritius</option>
                                    <option value="YT">Mayotte</option>
                                    <option value="MX">Mexico</option>
                                    <option value="FM">Micronesia, Federated States of</option>
                                    <option value="MD">Moldova, Republic of</option>
                                    <option value="MC">Monaco</option>
                                    <option value="MN">Mongolia</option>
                                    <option value="ME">Montenegro</option>
                                    <option value="MS">Montserrat</option>
                                    <option value="MA">Morocco</option>
                                    <option value="MZ">Mozambique</option>
                                    <option value="MM">Myanmar</option>
                                    <option value="NA">Namibia</option>
                                    <option value="NR">Nauru</option>
                                    <option value="NP">Nepal</option>
                                    <option value="NL">Netherlands</option>
                                    <option value="NC">New Caledonia</option>
                                    <option value="NZ">New Zealand</option>
                                    <option value="NI">Nicaragua</option>
                                    <option value="NE">Niger</option>
                                    <option value="NG">Nigeria</option>
                                    <option value="NU">Niue</option>
                                    <option value="NF">Norfolk Island</option>
                                    <option value="MP">Northern Mariana Islands</option>
                                    <option value="NO">Norway</option>
                                    <option value="OM">Oman</option>
                                    <option value="PK">Pakistan</option>
                                    <option value="PW">Palau</option>
                                    <option value="PS">Palestinian Territory, Occupied</option>
                                    <option value="PA">Panama</option>
                                    <option value="PG">Papua New Guinea</option>
                                    <option value="PY">Paraguay</option>
                                    <option value="PE">Peru</option>
                                    <option value="PH">Philippines</option>
                                    <option value="PN">Pitcairn</option>
                                    <option value="PL">Poland</option>
                                    <option value="PT">Portugal</option>
                                    <option value="PR">Puerto Rico</option>
                                    <option value="QA">Qatar</option>
                                    <option value="RE">Réunion</option>
                                    <option value="RO">Romania</option>
                                    <option value="RU">Russian Federation</option>
                                    <option value="RW">Rwanda</option>
                                    <option value="BL">Saint Barthélemy</option>
                                    <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                                    <option value="KN">Saint Kitts and Nevis</option>
                                    <option value="LC">Saint Lucia</option>
                                    <option value="MF">Saint Martin (French part)</option>
                                    <option value="PM">Saint Pierre and Miquelon</option>
                                    <option value="VC">Saint Vincent and the Grenadines</option>
                                    <option value="WS">Samoa</option>
                                    <option value="SM">San Marino</option>
                                    <option value="ST">Sao Tome and Principe</option>
                                    <option value="SA">Saudi Arabia</option>
                                    <option value="SN">Senegal</option>
                                    <option value="RS">Serbia</option>
                                    <option value="SC">Seychelles</option>
                                    <option value="SL">Sierra Leone</option>
                                    <option value="SG">Singapore</option>
                                    <option value="SX">Sint Maarten (Dutch part)</option>
                                    <option value="SK">Slovakia</option>
                                    <option value="SI">Slovenia</option>
                                    <option value="SB">Solomon Islands</option>
                                    <option value="SO">Somalia</option>
                                    <option value="ZA">South Africa</option>
                                    <option value="GS">South Georgia and the South Sandwich Islands</option>
                                    <option value="SS">South Sudan</option>
                                    <option value="ES">Spain</option>
                                    <option value="LK">Sri Lanka</option>
                                    <option value="SD">Sudan</option>
                                    <option value="SR">Suriname</option>
                                    <option value="SJ">Svalbard and Jan Mayen</option>
                                    <option value="SZ">Swaziland</option>
                                    <option value="SE">Sweden</option>
                                    <option value="CH">Switzerland</option>
                                    <option value="SY">Syrian Arab Republic</option>
                                    <option value="TW">Taiwan, Province of China</option>
                                    <option value="TJ">Tajikistan</option>
                                    <option value="TZ">Tanzania, United Republic of</option>
                                    <option value="TH" selected>Thailand</option>
                                    <option value="TL">Timor-Leste</option>
                                    <option value="TG">Togo</option>
                                    <option value="TK">Tokelau</option>
                                    <option value="TO">Tonga</option>
                                    <option value="TT">Trinidad and Tobago</option>
                                    <option value="TN">Tunisia</option>
                                    <option value="TR">Turkey</option>
                                    <option value="TM">Turkmenistan</option>
                                    <option value="TC">Turks and Caicos Islands</option>
                                    <option value="TV">Tuvalu</option>
                                    <option value="UG">Uganda</option>
                                    <option value="UA">Ukraine</option>
                                    <option value="AE">United Arab Emirates</option>
                                    <option value="GB">United Kingdom</option>
                                    <option value="US">United States</option>
                                    <option value="UM">United States Minor Outlying Islands</option>
                                    <option value="UY">Uruguay</option>
                                    <option value="UZ">Uzbekistan</option>
                                    <option value="VU">Vanuatu</option>
                                    <option value="VE">Venezuela, Bolivarian Republic of</option>
                                    <option value="VN">Viet Nam</option>
                                    <option value="VG">Virgin Islands, British</option>
                                    <option value="VI">Virgin Islands, U.S.</option>
                                    <option value="WF">Wallis and Futuna</option>
                                    <option value="EH">Western Sahara</option>
                                    <option value="YE">Yemen</option>
                                    <option value="ZM">Zambia</option>
                                    <option value="ZW">Zimbabwe</option>
                                </select>
{{--                                <input type="text" class="form-control" id="Country" name="country" placeholder="">--}}
                            </div>
                            <div class="form-group" style="align-content: center">
                                <div class="g-recaptcha" data-sitekey="6Lefb-4UAAAAANLfK5L9jnviZrWkP64ztR8LjlZ-"></div>
                            </div>
                            <div class="form-group">

                                <button style="width: 100%" type="submit" class="btn btn-success">Create Account</button>
{{--                                <a href="{{url('show')}}" style="width: 100%" type="submit" class="btn btn-success"> Create Account</a>--}}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {

        $("#email_error_message").hide();
        $("#password_error_message").hide();
        $("#password_confirmation_error_message").hide();
        $("#firstname_error_message").hide();
        $("#lastname_error_message").hide();
        $("#phone_number_error_message").hide();
        // $("#province_error_message").hide();

        var error_email = false;
        var error_password = false;
        var error_password_confirmation = false;
        var error_firstname = false;
        var error_lastname = false;
        var error_phone_number = false;
        // var error_province = false;

        $("#form_email").focusout(function () {
            check_email();
        });
        $("#form_password").focusout(function () {
            check_password();
        });
        $("#form_password_confirmation").focusout(function () {
            check_password_confirmation();
        });
        $("#form_firstname").focusout(function () {
            check_firstname();
        });
        $("#form_lastname").focusout(function () {
            check_lastname();
        });
        $("#form_phone_number").focusout(function () {
            check_phone_number();
        });
        // $("#form_province").focusout(function () {
        //     check_province();
        // });

        function check_firstname() {
            let pattern = /^[a-zA-Z]*$/;
            let firstname = $("#form_firstname").val()
            if (pattern.test(firstname) && firstname !== "") {
                $("#firstname_error_message").hide();
                $("#form_firstname").css("border-bottom","2px solid #34F458");
            } else {
                $("#firstname_error_message").html("Should contain only Characters");
                $("#firstname_error_message").show();
                $("#form_firstname").css("border-bottom","2px solid #F90A0A");
                error_firstname = true;
            }
        }
        function check_lastname() {
            let pattern = /^[a-zA-Z]*$/;
            let lastname = $("#form_lastname").val()
            if (pattern.test(lastname) && lastname !== "") {
                $("#lastname_error_message").hide();
                $("#form_lastname").css("border-bottom","2px solid #34F458");
            } else {
                $("#lastname_error_message").html("Should contain only Characters");
                $("#lastname_error_message").show();
                $("#form_lastname").css("border-bottom","2px solid #F90A0A");
                error_lastname = true;
            }
        }
        function check_password() {
            let password_length = $("#form_password").val().length;
            if (password_length < 6){
                $("#password_error_message").html("At least 6 Characters");
                $("#form_password").css("border-bottom","2px solid #F90A0A");
                $("#password_error_message").show();
                error_password = true;
            } else {
               $("#password_error_message").hide();
               $("#form_password").css("border-bottom","2px solid #34F458")
            }
        }
        function check_password_confirmation() {
            let password = $("#form_password").val();
            let password_confirmation = $("#form_password_confirmation").val();
            if (password !== password_confirmation){
                $("#password_confirmation_error_message").html("Passwords Did not Matched");
                $("#password_confirmation_error_message").show();
                $("#form_password_confirmation").css("border-bottom","2px solid #F90A0A");
                error_password_confirmation = true;
            } else {
                $("#password_confirmation_error_message").hide();
                $("#form_password_confirmation").css("border-bottom","2px solid #34F458")
            }
        }function check_email() {
            let pattern = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            let email = $("#form_email").val()
            if (pattern.test(email) && email !== "") {
                $("#email_error_message").hide();
                $("#form_email").css("border-bottom","2px solid #34F458");
            } else {
                $("#email_error_message").html("Invalid Email");
                $("#email_error_message").show();
                $("#form_email").css("border-bottom","2px solid #F90A0A");
                error_email = true;
            }
        }
        function check_phone_number() {
            let phonenumber = $("#form_phone_number").val()
            if (phonenumber !== "") {
                $("#phone_number_error_message").hide();
                $("#form_phone_number").css("border-bottom","2px solid #34F458");
            } else {
                $("#phone_number_error_message").html("Invalid Phone number");
                $("#phone_number_error_message").show();
                $("#form_phone_number").css("border-bottom","2px solid #F90A0A");
                error_lastname = true;
            }
        }

    })
</script>

{{--<script type="text/javascript">--}}
{{-- $(function () {--}}
{{--     $("form[name='registration']").validate({--}}
{{--         rules: {--}}
{{--             // The key name on the left side is the name attribute--}}
{{--             // of an input field. Validation rules are defined--}}
{{--             // on the right side--}}
{{--             firstname: "required",--}}
{{--             lastname: "required",--}}
{{--             email: {--}}
{{--                 required: true,--}}
{{--                 // Specify that email should be validated--}}
{{--                 // by the built-in "email" rule--}}
{{--                 email: true--}}
{{--             },--}}
{{--             password: {--}}
{{--                 required: true,--}}
{{--                 minlength: 5--}}
{{--             }--}}
{{--         },--}}
{{--         // Specify validation error messages--}}
{{--         messages: {--}}
{{--             firstname: "Please enter your firstname",--}}
{{--             lastname: "Please enter your lastname",--}}
{{--             password: {--}}
{{--                 required: "Please provide a password",--}}
{{--                 minlength: "Your password must be at least 5 characters long"--}}
{{--             },--}}
{{--             email: "Please enter a valid email address"--}}
{{--         },--}}
{{--     })--}}
{{-- })--}}
{{--</script>--}}
{{--<script--}}
{{--        src="https://code.jquery.com/jquery-3.5.0.js"--}}
{{--        integrity="sha256-r/AaFHrszJtwpe+tHyNi/XCfMxYpbsRg2Uqn0x3s2zc="--}}
{{--        crossorigin="anonymous"></script>--}}
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>
