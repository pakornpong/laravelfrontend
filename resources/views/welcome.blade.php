<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57"          href="{{url('fav/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60"          href="{{url('fav/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72"          href="{{url('fav/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76"          href="{{url('fav/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114"        href="{{url('fav/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120"        href="{{url('fav/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144"        href="{{url('fav/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152"        href="{{url('fav/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180"        href="{{url('fav/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"   href="{{url('fav/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32"     href="{{url('fav/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96"     href="{{url('fav/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16"     href="{{url('fav/favicon-16x16.png')}}">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title>Thai Baht Digital</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: Lato,'Helvetica Neue',Arial,Helvetica,sans-serif;
            font-weight: 400;
            height: 100vh;
            margin: 0;
            font-size: 14px;
            line-height: 1.4285em;
            color: rgba(0,0,0,.87);
            font-kerning: normal;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .ui.container {
            max-width: 100%!important;
            margin-left: auto!important;
            margin-right: auto!important;
            background-color: transparent;
            font-size: 1.07142857rem;
            border-radius: 0;
            width: 1127px;
        }
        .active.item {
            border-color: #fff;
            color: #fff!important;
        }
        .right.item {
            display: flex;
            margin-left: auto!important;
        }
        .ui.text.container {
            font-size: 1.14285714rem;
            line-height: 1.5;
        }
        .ui.grid {
            margin-top: -1rem;
            margin-bottom: -1rem;
            display: flex;
            flex-direction: row;
            align-items: stretch;
            padding: 0;
            flex-wrap: wrap;
        }
        .ui.grid.container {
            width: calc(1127px + 2rem)!important;
        }
        .eight.wide {
            width: 50%!important;
        }
        .ui.header {
            border: none;
            font-weight: 700;
            line-height: 1.28571429em;
            text-transform: none;
            color: rgba(0,0,0,.87);
            font-family: Lato,'Helvetica Neue',Arial,Helvetica,sans-serif;
        }
        h1 {
            min-height: 1rem;
            text-align: center;
            font-family: Lato,'Helvetica Neue',Arial,Helvetica,sans-serif;
        }
        h2 {
            text-align: center;
            font-family: Lato,'Helvetica Neue',Arial,Helvetica,sans-serif;
        }
        h3 {
            font-family: Lato,'Helvetica Neue',Arial,Helvetica,sans-serif;
            text-align: center
        }
        h4 {
            font-family: Lato,'Helvetica Neue',Arial,Helvetica,sans-serif;
            text-align: center;
            font-size: 1.047142857rem;
        }
        p {
            font-family: Lato,'Helvetica Neue',Arial,Helvetica,sans-serif;
            margin: 0 0 1em;
            line-height: 1.4285em;
            padding: 0;
        }
        .ui.vertical.segment {
            margin: 0;
            border-radius: 0;
            box-shadow: none;
            border: none;
            border-bottom: solid 1px rgba(34,36,38,.15);
            background: none transparent;
        }
        .ui.inverted.segment {
            background: #1b1c1d;
            color: rgba(255,255,255,.9);
            border: none;
        }
        .ui.text.container {
            font-size: 1.14285714rem;
            line-height: 1.5;
        }
        .ui.large {
            font-size: 1.14rem;
        }
        .ui.button {
            cursor: pointer;
            display: inline-block ;
            min-height: 1em;
            outline: 0;
            border: none;
            vertical-align: baseline;
            background: #e0e1e2 none;
            color: rgba(0,0,0,.6);
            margin: 0 .25em 0 0;
            padding: .7857em 1.5em .7857em;
            text-transform: none;
            text-shadow: none;
            font-weight: 700;
            line-height: 1em;
            font-style: normal;
            text-align: center;
            text-decoration: none;
            border-radius: .28571429rem;
            box-shadow: 0 0 0 1px transparent inset, 0 0 0 0 rgba(34,36,38,.15) inset;
        }
        .ui.segment {
            font-size: 1rem;
            position: relative;
        }
        .ui.list {
            font-size: 1em;
            list-style-type: none;
            margin: 1em 0;
            padding-bottom: 0;
        }
        .ui.celled.grid {
            width: 100%;
        }
        .ui.inverted.link.list.item {
            color: rgba(255,255,255,.5);
        }
        .ui.secondary.pointing.menu {
            margin-right: 0;
            margin-left: 0;
        }
        .ui.secondary.inverted.menu {
            background-color: transparent;
        }
        .ui.large.menu {
            font-size: 1.07142857rem;
        }
        .ui.secondary.pointing.menu .item {
            border-radius: 0;
            align-self: flex-end;
            margin: 0 0 -2px;
            padding: .85714286em 1.14285714em;
            border-bottom-width: 2px;
            transition: color .1s ease;
        }
        .ui.inverted.button {
            box-shadow: 0 0 0 2px #fff inset!important;
            background: transparent none;
            color: white;
            text-shadow: none!important;
        }
        .ui.inverted.header {
            color: #fff;
        }
        .container {
            padding-left: 0;
            padding-right: 0;
        }
        .three.wide {
            width: 18.75%!important;
        }
        .five.wide {
            width: 20.75%!important;
        }
        .seven.wide {
            width: 43.75%!important;
        }
        .eight.wide {
            width: 50%!important;
            box-sizing: border-box;
        }
        .ui.tiny.image {
            width: 80px;
            height: auto;
            font-size: .85714286rem;
        }
        .ui.centered.image {
            margin-left: auto;
            margin-right: auto;
        }
        .ui.rounded.image {
            border-radius: .3125em;
        }

        .firstSection{
            background-image: url({{url('img/bg.jpg')}});
            background-size: cover;
            background-position:center;

        }

        .nav-link{
            color: white;
            margin: 0 0 -2px;
            padding: .85714286em 1.14285714em;
        }

        .link-active{
            border-color: #fff;
            border-radius: .28571429rem;
            border-bottom-width: 2px;
            border-bottom-color: transparent;
            border-bottom-style: solid;
            border-radius: 0;
            -ms-flex-item-align: end;
            align-self: flex-end;

            border-bottom-width: 2px;
            -webkit-transition: color .1s ease;
            transition: color .1s ease;
        }
        .sticky {
            position: fixed;
            top: 0;
            width: 100%;
        }


        @keyframes frames {
            50% { background:red;  }
        }

        ul.timeline {
            list-style-type: none;
            position: relative;
        }
        ul.timeline:before {
            content: ' ';
            background: #d4d9df;
            display: inline-block;
            position: absolute;
            left: 29px;
            width: 2px;
            height: 100%;
            z-index: 400;
        }
        ul.timeline > li {
            margin: 20px 0;
            padding-left: 20px;
        }
        ul.timeline > li:before {
            content: ' ';
            background: white;
            display: inline-block;
            position: absolute;
            border-radius: 50%;
            border: 3px solid #22c0e8;
            left: 20px;
            width: 20px;
            height: 20px;
            z-index: 400;
        }

        .nav-link{
            padding-left: 0px;
        }

    </style>
</head>
<body>

<div class="sticky"  style="z-index: 999;  background: rgba(0, 0, 0, 0.4);">
    <nav class="navbar navbar-expand-sm">
{{--        <button style="border: none" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">--}}
{{--            <span>Menu</span>--}}
{{--        </button>--}}

            <ul class="nav nav-pills">
                <li class="nav-item">
                    <a class="nav-link" href="{{url('register')}}"> Register </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('TBD-Whitepaper-[v.3.2].pdf')}}">White Paper</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('TBD-AML-Policy.pdf')}}">AML Policy</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('contact')}}">Contact Us</a>
                </li>
            </ul>

    </nav>
</div>


<div class="firstSection" tabindex="-1" style="margin-top: -60px; height: calc(100vh + 60px); ">
        <!-- content ในรูป -->
        <div class="ui text container" >
           <div class=" ui inverted header"
           style="text-align: center; font-size: calc(38px + (26 - 14) * ((100vw - 300px) / (1600 - 300)));
            font-weight:normal;
            padding-top: 30vh;">
               <img src="{{url('img/TBD_Logo.png')}}"> <br>
               <span>A <span style="color:#0476fb">relevance token</span> made for Digital Age</span></div>
            <div style="text-align: center;
            font-size:1.7em;
            font-weight:normal;
            margin-top:1em;
            margin-bottom:1.5em"
                 class="ui inverted header">
                Unlocking Digital Economy
            </div>
        </div>
</div>


<div class="container ">
    <div class="row" style="margin: 0px;padding-top: 50px; padding-bottom: 50px;">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div style="font-size: 1.8em;text-align: center;padding-bottom: 30px;" class="ui header centered">
            A transparent, auditable, smart-contract based relevance value token</div>
            <div style="font-size:1.2em;color: black; line-height: 1.4em">
                Thai Baht Digital (TBD) is a cryptographic token that is strictly pegged 1:1 to the Thai Baht. Being a relevance value token, TBD utilizes the benefits of cryptocurrency properties as well as trustworthiness and low volatility of the national currency. TBD is issued as an ERC20 on the Ethereum network, where it is trusted by a transparent fiat-pegged mechanism and an auditable reserve custody. TBD opens possibilities in trading, payment and many more use cases in a digital economy.
            </div>
        </div>
        <div class="d-block d-sm-block d-md-none  col-sm-12 ui vertical segment" style="padding-bottom: 30px;"></div>
        <div class="d-block d-sm-block d-md-none  col-sm-12 " style="height: 30px;"></div>
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div style="font-size: 1.8em;text-align: center;padding-bottom: 30px;" class="ui header">
               Road Map</div>
            <ul class="timeline">
                <li>
                    <a>2020 Q2</a>
                    <a href="#" class="float-right">   <img src="{{url("img/ex.svg")}}" style="width: 35px;" class="ui tiny centered rounded image"></a>
                    <p>Listing TBD on Exchange</p>
                </li>
                <li>
                    <a>2020 Q3-4</a>
                    <a href="#" class="float-right">   <img src="{{url("img/reg.svg")}}" style="width: 35px;" class="ui tiny centered rounded image"></a>
                    <p>Apply Dealer License</p>
                </li>
                <li>
                    <a>2021 Q1-2</a>
                    <a href="#" class="float-right">
                        <img src="data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjUxMnB0IiB2aWV3Qm94PSIwIC04NSA1MTIgNTEyIiB3aWR0aD0iNTEycHQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0ibTI5OS45MjU3ODEgMTY1LjIwNzAzMWM1Ljg4NjcxOS04LjI3NzM0MyA5LjQwNjI1LTE4LjMyODEyNSA5LjQwNjI1LTI5LjIwNzAzMSAwLTI1LjE5NTMxMi0xOC41MzkwNjItNDUuOTk2MDk0LTQyLjY2NDA2Mi00OS44NTU0Njl2LTYuMTQ0NTMxYzAtOC44MzIwMzEtNy4xNjc5NjktMTYtMTYtMTZzLTE2IDcuMTY3OTY5LTE2IDE2djUuMzMyMDMxaC0yNi42Njc5NjljLTguODMyMDMxIDAtMTYgNy4xNjc5NjktMTYgMTZ2MTM4LjY2Nzk2OWMwIDguODMyMDMxIDcuMTY3OTY5IDE2IDE2IDE2aDI2LjY2Nzk2OXY1LjMzMjAzMWMwIDguODMyMDMxIDcuMTY3OTY5IDE2IDE2IDE2czE2LTcuMTY3OTY5IDE2LTE2di01LjMzMjAzMWgyLjY2NDA2MmMyNy45NDkyMTkgMCA1MC42Njc5NjktMjIuNzE4NzUgNTAuNjY3OTY5LTUwLjY2Nzk2OSAwLTE2LjQwMjM0My03Ljk1NzAzMS0zMC44NDc2NTYtMjAuMDc0MjE5LTQwLjEyNXptLTc1LjkyNTc4MS00Ny44NzVoMzQuNjY3OTY5YzEwLjI4MTI1IDAgMTguNjY0MDYyIDguMzg2NzE5IDE4LjY2NDA2MiAxOC42Njc5NjlzLTguMzgyODEyIDE4LjY2Nzk2OS0xOC42NjQwNjIgMTguNjY3OTY5aC0zNC42Njc5Njl6bTQ1LjMzMjAzMSAxMDYuNjY3OTY5aC00NS4zMzIwMzF2LTM3LjMzMjAzMWg0NS4zMzIwMzFjMTAuMjg1MTU3IDAgMTguNjY3OTY5IDguMzgyODEyIDE4LjY2Nzk2OSAxOC42NjQwNjIgMCAxMC4yODUxNTctOC4zODI4MTIgMTguNjY3OTY5LTE4LjY2Nzk2OSAxOC42Njc5Njl6bTAgMCIvPjxwYXRoIGQ9Im00NTMuMzMyMDMxIDBoLTM5NC42NjQwNjJjLTMyLjM2MzI4MSAwLTU4LjY2Nzk2OSAyNi4zMDQ2ODgtNTguNjY3OTY5IDU4LjY2Nzk2OXYyMjRjMCAzMi4zNjMyODEgMjYuMzA0Njg4IDU4LjY2NDA2MiA1OC42Njc5NjkgNTguNjY0MDYyaDM5NC42NjQwNjJjMzIuMzYzMjgxIDAgNTguNjY3OTY5LTI2LjMwMDc4MSA1OC42Njc5NjktNTguNjY0MDYydi0yMjRjMC0zMi4zNjMyODEtMjYuMzA0Njg4LTU4LjY2Nzk2OS01OC42Njc5NjktNTguNjY3OTY5em0yNi42Njc5NjkgMjgyLjY2Nzk2OWMwIDE0LjY5OTIxOS0xMS45Njg3NSAyNi42NjQwNjItMjYuNjY3OTY5IDI2LjY2NDA2MmgtMzk0LjY2NDA2MmMtMTQuNjk5MjE5IDAtMjYuNjY3OTY5LTExLjk2NDg0My0yNi42Njc5NjktMjYuNjY0MDYydi0yMjRjMC0xNC42OTkyMTkgMTEuOTY4NzUtMjYuNjY3OTY5IDI2LjY2Nzk2OS0yNi42Njc5NjloMzk0LjY2NDA2MmMxNC42OTkyMTkgMCAyNi42Njc5NjkgMTEuOTY4NzUgMjYuNjY3OTY5IDI2LjY2Nzk2OXptMCAwIi8+PHBhdGggZD0ibTEzMy4zMzIwMzEgNjRoLTUzLjMzMjAzMWMtOC44MzIwMzEgMC0xNiA3LjE2Nzk2OS0xNiAxNnM3LjE2Nzk2OSAxNiAxNiAxNmg1My4zMzIwMzFjOC44MzIwMzEgMCAxNi03LjE2Nzk2OSAxNi0xNnMtNy4xNjc5NjktMTYtMTYtMTZ6bTAgMCIvPjxwYXRoIGQ9Im00MzIgMjQ1LjMzMjAzMWgtNTMuMzMyMDMxYy04LjgzMjAzMSAwLTE2IDcuMTY3OTY5LTE2IDE2czcuMTY3OTY5IDE2IDE2IDE2aDUzLjMzMjAzMWM4LjgzMjAzMSAwIDE2LTcuMTY3OTY5IDE2LTE2cy03LjE2Nzk2OS0xNi0xNi0xNnptMCAwIi8+PC9zdmc+" style="width: 35px;"
                             class="ui tiny centered rounded image">
                    </a>
                    <p>Opening TBD OTC (*Subject to SEC)</p>
                </li>
                <li>
                    <a>2021 Q3-4</a>
                    <a href="#" class="float-right">
                        <img src="{{url("img/dev.svg")}}" style="width: 35px;" class="ui tiny centered rounded image">
                    </a>
                    <p>Start our Mainnet</p>
                </li>
                <li>
                    <a>2022</a>
                    <a href="#" class="float-right">
                        <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJDYXBhXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB2aWV3Qm94PSIwIDAgNTggNTgiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDU4IDU4OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8cGF0aCBkPSJNNTAuNjg4LDQ4LjIyMkM1NS4yMzIsNDMuMTAxLDU4LDM2LjM2OSw1OCwyOWMwLTcuNjY3LTIuOTk2LTE0LjY0My03Ljg3Mi0xOS44MzRjMCwwLDAtMC4wMDEsMC0wLjAwMQ0KCWMtMC4wMDQtMC4wMDYtMC4wMS0wLjAwOC0wLjAxMy0wLjAxM2MtNS4wNzktNS4zOTktMTIuMTk1LTguODU1LTIwLjExLTkuMTI2bC0wLjAwMS0wLjAwMUwyOS40MzksMC4wMUMyOS4yOTMsMC4wMDUsMjkuMTQ3LDAsMjksMA0KCXMtMC4yOTMsMC4wMDUtMC40MzksMC4wMWwtMC41NjMsMC4wMTVsLTAuMDAxLDAuMDAxYy03LjkxNSwwLjI3MS0xNS4wMzEsMy43MjctMjAuMTEsOS4xMjZjLTAuMDA0LDAuMDA1LTAuMDEsMC4wMDctMC4wMTMsMC4wMTMNCgljMCwwLDAsMC4wMDEtMC4wMDEsMC4wMDJDMi45OTYsMTQuMzU3LDAsMjEuMzMzLDAsMjljMCw3LjM2OSwyLjc2OCwxNC4xMDEsNy4zMTIsMTkuMjIyYzAuMDA2LDAuMDA5LDAuMDA2LDAuMDE5LDAuMDEzLDAuMDI4DQoJYzAuMDE4LDAuMDI1LDAuMDQ0LDAuMDM3LDAuMDYzLDAuMDZjNS4xMDYsNS43MDgsMTIuNDMyLDkuMzg1LDIwLjYwOCw5LjY2NWwwLjAwMSwwLjAwMWwwLjU2MywwLjAxNUMyOC43MDcsNTcuOTk1LDI4Ljg1Myw1OCwyOSw1OA0KCXMwLjI5My0wLjAwNSwwLjQzOS0wLjAxbDAuNTYzLTAuMDE1bDAuMDAxLTAuMDAxYzguMTg1LTAuMjgxLDE1LjUxOS0zLjk2NSwyMC42MjUtOS42ODVjMC4wMTMtMC4wMTcsMC4wMzQtMC4wMjIsMC4wNDYtMC4wNA0KCUM1MC42ODIsNDguMjQxLDUwLjY4Miw0OC4yMzEsNTAuNjg4LDQ4LjIyMnogTTIuMDI1LDMwaDEyLjAwM2MwLjExMyw0LjIzOSwwLjk0MSw4LjM1OCwyLjQxNSwxMi4yMTcNCgljLTIuODQ0LDEuMDI5LTUuNTYzLDIuNDA5LTguMTExLDQuMTMxQzQuNTg1LDQxLjg5MSwyLjI1MywzNi4yMSwyLjAyNSwzMHogTTguODc4LDExLjAyM2MyLjQ4OCwxLjYxOCw1LjEzNywyLjkxNCw3LjksMy44ODINCglDMTUuMDg2LDE5LjAxMiwxNC4xNSwyMy40NCwxNC4wMjgsMjhIMi4wMjVDMi4yNjQsMjEuNDkzLDQuODEyLDE1LjU2OCw4Ljg3OCwxMS4wMjN6IE01NS45NzUsMjhINDMuOTcyDQoJYy0wLjEyMi00LjU2LTEuMDU4LTguOTg4LTIuNzUtMTMuMDk1YzIuNzYzLTAuOTY4LDUuNDEyLTIuMjY0LDcuOS0zLjg4MkM1My4xODgsMTUuNTY4LDU1LjczNiwyMS40OTMsNTUuOTc1LDI4eiBNMjgsMTQuOTYzDQoJYy0yLjg5MS0wLjA4Mi01LjcyOS0wLjUxMy04LjQ3MS0xLjI4M0MyMS41NTYsOS41MjIsMjQuNDE4LDUuNzY5LDI4LDIuNjQ0VjE0Ljk2M3ogTTI4LDE2Ljk2M1YyOEgxNi4wMjgNCgljMC4xMjMtNC4zNDgsMS4wMzUtOC41NjUsMi42NjYtMTIuNDc1QzIxLjcsMTYuMzk2LDI0LjgyMSwxNi44NzgsMjgsMTYuOTYzeiBNMzAsMTYuOTYzYzMuMTc5LTAuMDg1LDYuMy0wLjU2Niw5LjMwNy0xLjQzOA0KCWMxLjYzMSwzLjkxLDIuNTQzLDguMTI3LDIuNjY2LDEyLjQ3NUgzMFYxNi45NjN6IE0zMCwxNC45NjNWMi42NDRjMy41ODIsMy4xMjUsNi40NDQsNi44NzgsOC40NzEsMTEuMDM2DQoJQzM1LjcyOSwxNC40NSwzMi44OTEsMTQuODgxLDMwLDE0Ljk2M3ogTTQwLjQwOSwxMy4wNzJjLTEuOTIxLTQuMDI1LTQuNTg3LTcuNjkyLTcuODg4LTEwLjgzNQ0KCWM1Ljg1NiwwLjc2NiwxMS4xMjUsMy40MTQsMTUuMTgzLDcuMzE4QzQ1LjQsMTEuMDE3LDQyLjk1NiwxMi4xOTMsNDAuNDA5LDEzLjA3MnogTTE3LjU5MSwxMy4wNzINCgljLTIuNTQ3LTAuODc5LTQuOTkxLTIuMDU1LTcuMjk0LTMuNTE3YzQuMDU3LTMuOTA0LDkuMzI3LTYuNTUyLDE1LjE4My03LjMxOEMyMi4xNzgsNS4zOCwxOS41MTIsOS4wNDcsMTcuNTkxLDEzLjA3MnogTTE2LjAyOCwzMA0KCUgyOHYxMC4wMzhjLTMuMzA3LDAuMDg4LTYuNTQ3LDAuNjA0LTkuNjYxLDEuNTQxQzE2LjkzMiwzNy45MjQsMTYuMTQxLDM0LjAxOSwxNi4wMjgsMzB6IE0yOCw0Mi4wMzh2MTMuMzE4DQoJYy0zLjgzNC0zLjM0NS02Ljg0LTcuNDA5LTguODg0LTExLjkxN0MyMS45ODMsNDIuNTk0LDI0Ljk2MSw0Mi4xMjQsMjgsNDIuMDM4eiBNMzAsNTUuMzU2VjQyLjAzOA0KCWMzLjAzOSwwLjA4NSw2LjAxNywwLjU1Niw4Ljg4NCwxLjRDMzYuODQsNDcuOTQ3LDMzLjgzNCw1Mi4wMTEsMzAsNTUuMzU2eiBNMzAsNDAuMDM4VjMwaDExLjk3Mg0KCWMtMC4xMTMsNC4wMTktMC45MDQsNy45MjQtMi4zMTIsMTEuNThDMzYuNTQ3LDQwLjY0MiwzMy4zMDcsNDAuMTI2LDMwLDQwLjAzOHogTTQzLjk3MiwzMGgxMi4wMDMNCgljLTAuMjI4LDYuMjEtMi41NTksMTEuODkxLTYuMzA3LDE2LjM0OGMtMi41NDgtMS43MjItNS4yNjctMy4xMDItOC4xMTEtNC4xMzFDNDMuMDMyLDM4LjM1OCw0My44NTksMzQuMjM5LDQzLjk3MiwzMHoNCgkgTTkuNjkxLDQ3Ljg0NmMyLjM2Ni0xLjU3Miw0Ljg4NS0yLjgzNiw3LjUxNy0zLjc4MWMxLjk0NSw0LjM2LDQuNzM3LDguMzMzLDguMjcxLDExLjY5OEMxOS4zMjgsNTQuOTU4LDEzLjgyMyw1Mi4wNzgsOS42OTEsNDcuODQ2DQoJeiBNMzIuNTIxLDU1Ljc2M2MzLjUzNC0zLjM2NCw2LjMyNi03LjMzNyw4LjI3MS0xMS42OThjMi42MzIsMC45NDUsNS4xNSwyLjIwOSw3LjUxNywzLjc4MQ0KCUM0NC4xNzcsNTIuMDc4LDM4LjY3Miw1NC45NTgsMzIuNTIxLDU1Ljc2M3oiLz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjwvc3ZnPg0K" style="width: 35px;"
                             class="ui tiny centered rounded image">
                    </a>
                    <p>Expand usage to Asia</p>
                </li>
            </ul>
        </div>
    </div>
</div>



<div class="ui vertical segment"></div>

<div class="container" >
    <div class="row" style="margin: 0px; padding-top: 50px;">
        <div class="col-md-4 col-sm-12" style="text-align: center;">
            <img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJDYXBhXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4Ig0KCSB2aWV3Qm94PSIwIDAgNTggNTgiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDU4IDU4OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8cGF0aCBkPSJNNTAuNjg4LDQ4LjIyMkM1NS4yMzIsNDMuMTAxLDU4LDM2LjM2OSw1OCwyOWMwLTcuNjY3LTIuOTk2LTE0LjY0My03Ljg3Mi0xOS44MzRjMCwwLDAtMC4wMDEsMC0wLjAwMQ0KCWMtMC4wMDQtMC4wMDYtMC4wMS0wLjAwOC0wLjAxMy0wLjAxM2MtNS4wNzktNS4zOTktMTIuMTk1LTguODU1LTIwLjExLTkuMTI2bC0wLjAwMS0wLjAwMUwyOS40MzksMC4wMUMyOS4yOTMsMC4wMDUsMjkuMTQ3LDAsMjksMA0KCXMtMC4yOTMsMC4wMDUtMC40MzksMC4wMWwtMC41NjMsMC4wMTVsLTAuMDAxLDAuMDAxYy03LjkxNSwwLjI3MS0xNS4wMzEsMy43MjctMjAuMTEsOS4xMjZjLTAuMDA0LDAuMDA1LTAuMDEsMC4wMDctMC4wMTMsMC4wMTMNCgljMCwwLDAsMC4wMDEtMC4wMDEsMC4wMDJDMi45OTYsMTQuMzU3LDAsMjEuMzMzLDAsMjljMCw3LjM2OSwyLjc2OCwxNC4xMDEsNy4zMTIsMTkuMjIyYzAuMDA2LDAuMDA5LDAuMDA2LDAuMDE5LDAuMDEzLDAuMDI4DQoJYzAuMDE4LDAuMDI1LDAuMDQ0LDAuMDM3LDAuMDYzLDAuMDZjNS4xMDYsNS43MDgsMTIuNDMyLDkuMzg1LDIwLjYwOCw5LjY2NWwwLjAwMSwwLjAwMWwwLjU2MywwLjAxNUMyOC43MDcsNTcuOTk1LDI4Ljg1Myw1OCwyOSw1OA0KCXMwLjI5My0wLjAwNSwwLjQzOS0wLjAxbDAuNTYzLTAuMDE1bDAuMDAxLTAuMDAxYzguMTg1LTAuMjgxLDE1LjUxOS0zLjk2NSwyMC42MjUtOS42ODVjMC4wMTMtMC4wMTcsMC4wMzQtMC4wMjIsMC4wNDYtMC4wNA0KCUM1MC42ODIsNDguMjQxLDUwLjY4Miw0OC4yMzEsNTAuNjg4LDQ4LjIyMnogTTIuMDI1LDMwaDEyLjAwM2MwLjExMyw0LjIzOSwwLjk0MSw4LjM1OCwyLjQxNSwxMi4yMTcNCgljLTIuODQ0LDEuMDI5LTUuNTYzLDIuNDA5LTguMTExLDQuMTMxQzQuNTg1LDQxLjg5MSwyLjI1MywzNi4yMSwyLjAyNSwzMHogTTguODc4LDExLjAyM2MyLjQ4OCwxLjYxOCw1LjEzNywyLjkxNCw3LjksMy44ODINCglDMTUuMDg2LDE5LjAxMiwxNC4xNSwyMy40NCwxNC4wMjgsMjhIMi4wMjVDMi4yNjQsMjEuNDkzLDQuODEyLDE1LjU2OCw4Ljg3OCwxMS4wMjN6IE01NS45NzUsMjhINDMuOTcyDQoJYy0wLjEyMi00LjU2LTEuMDU4LTguOTg4LTIuNzUtMTMuMDk1YzIuNzYzLTAuOTY4LDUuNDEyLTIuMjY0LDcuOS0zLjg4MkM1My4xODgsMTUuNTY4LDU1LjczNiwyMS40OTMsNTUuOTc1LDI4eiBNMjgsMTQuOTYzDQoJYy0yLjg5MS0wLjA4Mi01LjcyOS0wLjUxMy04LjQ3MS0xLjI4M0MyMS41NTYsOS41MjIsMjQuNDE4LDUuNzY5LDI4LDIuNjQ0VjE0Ljk2M3ogTTI4LDE2Ljk2M1YyOEgxNi4wMjgNCgljMC4xMjMtNC4zNDgsMS4wMzUtOC41NjUsMi42NjYtMTIuNDc1QzIxLjcsMTYuMzk2LDI0LjgyMSwxNi44NzgsMjgsMTYuOTYzeiBNMzAsMTYuOTYzYzMuMTc5LTAuMDg1LDYuMy0wLjU2Niw5LjMwNy0xLjQzOA0KCWMxLjYzMSwzLjkxLDIuNTQzLDguMTI3LDIuNjY2LDEyLjQ3NUgzMFYxNi45NjN6IE0zMCwxNC45NjNWMi42NDRjMy41ODIsMy4xMjUsNi40NDQsNi44NzgsOC40NzEsMTEuMDM2DQoJQzM1LjcyOSwxNC40NSwzMi44OTEsMTQuODgxLDMwLDE0Ljk2M3ogTTQwLjQwOSwxMy4wNzJjLTEuOTIxLTQuMDI1LTQuNTg3LTcuNjkyLTcuODg4LTEwLjgzNQ0KCWM1Ljg1NiwwLjc2NiwxMS4xMjUsMy40MTQsMTUuMTgzLDcuMzE4QzQ1LjQsMTEuMDE3LDQyLjk1NiwxMi4xOTMsNDAuNDA5LDEzLjA3MnogTTE3LjU5MSwxMy4wNzINCgljLTIuNTQ3LTAuODc5LTQuOTkxLTIuMDU1LTcuMjk0LTMuNTE3YzQuMDU3LTMuOTA0LDkuMzI3LTYuNTUyLDE1LjE4My03LjMxOEMyMi4xNzgsNS4zOCwxOS41MTIsOS4wNDcsMTcuNTkxLDEzLjA3MnogTTE2LjAyOCwzMA0KCUgyOHYxMC4wMzhjLTMuMzA3LDAuMDg4LTYuNTQ3LDAuNjA0LTkuNjYxLDEuNTQxQzE2LjkzMiwzNy45MjQsMTYuMTQxLDM0LjAxOSwxNi4wMjgsMzB6IE0yOCw0Mi4wMzh2MTMuMzE4DQoJYy0zLjgzNC0zLjM0NS02Ljg0LTcuNDA5LTguODg0LTExLjkxN0MyMS45ODMsNDIuNTk0LDI0Ljk2MSw0Mi4xMjQsMjgsNDIuMDM4eiBNMzAsNTUuMzU2VjQyLjAzOA0KCWMzLjAzOSwwLjA4NSw2LjAxNywwLjU1Niw4Ljg4NCwxLjRDMzYuODQsNDcuOTQ3LDMzLjgzNCw1Mi4wMTEsMzAsNTUuMzU2eiBNMzAsNDAuMDM4VjMwaDExLjk3Mg0KCWMtMC4xMTMsNC4wMTktMC45MDQsNy45MjQtMi4zMTIsMTEuNThDMzYuNTQ3LDQwLjY0MiwzMy4zMDcsNDAuMTI2LDMwLDQwLjAzOHogTTQzLjk3MiwzMGgxMi4wMDMNCgljLTAuMjI4LDYuMjEtMi41NTksMTEuODkxLTYuMzA3LDE2LjM0OGMtMi41NDgtMS43MjItNS4yNjctMy4xMDItOC4xMTEtNC4xMzFDNDMuMDMyLDM4LjM1OCw0My44NTksMzQuMjM5LDQzLjk3MiwzMHoNCgkgTTkuNjkxLDQ3Ljg0NmMyLjM2Ni0xLjU3Miw0Ljg4NS0yLjgzNiw3LjUxNy0zLjc4MWMxLjk0NSw0LjM2LDQuNzM3LDguMzMzLDguMjcxLDExLjY5OEMxOS4zMjgsNTQuOTU4LDEzLjgyMyw1Mi4wNzgsOS42OTEsNDcuODQ2DQoJeiBNMzIuNTIxLDU1Ljc2M2MzLjUzNC0zLjM2NCw2LjMyNi03LjMzNyw4LjI3MS0xMS42OThjMi42MzIsMC45NDUsNS4xNSwyLjIwOSw3LjUxNywzLjc4MQ0KCUM0NC4xNzcsNTIuMDc4LDM4LjY3Miw1NC45NTgsMzIuNTIxLDU1Ljc2M3oiLz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjwvc3ZnPg0K"
                 class="ui tiny centered rounded image">
            <h3 style="font-size: 2em"
                class="ui header"
            >The best of both worlds - Fiat & Cryptocurrency</h3>
            <p style="font-size: 1.3em">
                Cryptocurrency provides a borderless and trustless value transfer features to money, however it is impractical to use as an effective medium of exchange due to the price volatility and trustworthiness problem of a new currency. To solve this, TBD provides a price relevance token pegged to Thai baht, a national currency that people already trust. This combines the benefits of both fiat currency and cryptocurrency together, which potentially leading to the real mainstream adoption.
            </p>
        </div>
        <div class="d-block d-sm-block d-md-none  col-sm-12 ui vertical segment" style="padding-bottom: 30px;"></div>
        <div class="d-block d-sm-block d-md-none  col-sm-12 " style="height: 30px;"></div>
        <div class="col-md-4 col-sm-12" style="text-align: center; ">
            <img src="data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjUxMnB0IiB2aWV3Qm94PSIwIC04NSA1MTIgNTEyIiB3aWR0aD0iNTEycHQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0ibTI5OS45MjU3ODEgMTY1LjIwNzAzMWM1Ljg4NjcxOS04LjI3NzM0MyA5LjQwNjI1LTE4LjMyODEyNSA5LjQwNjI1LTI5LjIwNzAzMSAwLTI1LjE5NTMxMi0xOC41MzkwNjItNDUuOTk2MDk0LTQyLjY2NDA2Mi00OS44NTU0Njl2LTYuMTQ0NTMxYzAtOC44MzIwMzEtNy4xNjc5NjktMTYtMTYtMTZzLTE2IDcuMTY3OTY5LTE2IDE2djUuMzMyMDMxaC0yNi42Njc5NjljLTguODMyMDMxIDAtMTYgNy4xNjc5NjktMTYgMTZ2MTM4LjY2Nzk2OWMwIDguODMyMDMxIDcuMTY3OTY5IDE2IDE2IDE2aDI2LjY2Nzk2OXY1LjMzMjAzMWMwIDguODMyMDMxIDcuMTY3OTY5IDE2IDE2IDE2czE2LTcuMTY3OTY5IDE2LTE2di01LjMzMjAzMWgyLjY2NDA2MmMyNy45NDkyMTkgMCA1MC42Njc5NjktMjIuNzE4NzUgNTAuNjY3OTY5LTUwLjY2Nzk2OSAwLTE2LjQwMjM0My03Ljk1NzAzMS0zMC44NDc2NTYtMjAuMDc0MjE5LTQwLjEyNXptLTc1LjkyNTc4MS00Ny44NzVoMzQuNjY3OTY5YzEwLjI4MTI1IDAgMTguNjY0MDYyIDguMzg2NzE5IDE4LjY2NDA2MiAxOC42Njc5NjlzLTguMzgyODEyIDE4LjY2Nzk2OS0xOC42NjQwNjIgMTguNjY3OTY5aC0zNC42Njc5Njl6bTQ1LjMzMjAzMSAxMDYuNjY3OTY5aC00NS4zMzIwMzF2LTM3LjMzMjAzMWg0NS4zMzIwMzFjMTAuMjg1MTU3IDAgMTguNjY3OTY5IDguMzgyODEyIDE4LjY2Nzk2OSAxOC42NjQwNjIgMCAxMC4yODUxNTctOC4zODI4MTIgMTguNjY3OTY5LTE4LjY2Nzk2OSAxOC42Njc5Njl6bTAgMCIvPjxwYXRoIGQ9Im00NTMuMzMyMDMxIDBoLTM5NC42NjQwNjJjLTMyLjM2MzI4MSAwLTU4LjY2Nzk2OSAyNi4zMDQ2ODgtNTguNjY3OTY5IDU4LjY2Nzk2OXYyMjRjMCAzMi4zNjMyODEgMjYuMzA0Njg4IDU4LjY2NDA2MiA1OC42Njc5NjkgNTguNjY0MDYyaDM5NC42NjQwNjJjMzIuMzYzMjgxIDAgNTguNjY3OTY5LTI2LjMwMDc4MSA1OC42Njc5NjktNTguNjY0MDYydi0yMjRjMC0zMi4zNjMyODEtMjYuMzA0Njg4LTU4LjY2Nzk2OS01OC42Njc5NjktNTguNjY3OTY5em0yNi42Njc5NjkgMjgyLjY2Nzk2OWMwIDE0LjY5OTIxOS0xMS45Njg3NSAyNi42NjQwNjItMjYuNjY3OTY5IDI2LjY2NDA2MmgtMzk0LjY2NDA2MmMtMTQuNjk5MjE5IDAtMjYuNjY3OTY5LTExLjk2NDg0My0yNi42Njc5NjktMjYuNjY0MDYydi0yMjRjMC0xNC42OTkyMTkgMTEuOTY4NzUtMjYuNjY3OTY5IDI2LjY2Nzk2OS0yNi42Njc5NjloMzk0LjY2NDA2MmMxNC42OTkyMTkgMCAyNi42Njc5NjkgMTEuOTY4NzUgMjYuNjY3OTY5IDI2LjY2Nzk2OXptMCAwIi8+PHBhdGggZD0ibTEzMy4zMzIwMzEgNjRoLTUzLjMzMjAzMWMtOC44MzIwMzEgMC0xNiA3LjE2Nzk2OS0xNiAxNnM3LjE2Nzk2OSAxNiAxNiAxNmg1My4zMzIwMzFjOC44MzIwMzEgMCAxNi03LjE2Nzk2OSAxNi0xNnMtNy4xNjc5NjktMTYtMTYtMTZ6bTAgMCIvPjxwYXRoIGQ9Im00MzIgMjQ1LjMzMjAzMWgtNTMuMzMyMDMxYy04LjgzMjAzMSAwLTE2IDcuMTY3OTY5LTE2IDE2czcuMTY3OTY5IDE2IDE2IDE2aDUzLjMzMjAzMWM4LjgzMjAzMSAwIDE2LTcuMTY3OTY5IDE2LTE2cy03LjE2Nzk2OS0xNi0xNi0xNnptMCAwIi8+PC9zdmc+"
                 class="ui tiny centered rounded image">
            <h3 style="font-size: 2em" class="ui header">Fiat-pegged Token</h3>
            <p style="font-size: 1.2em">
                TBD deploys a fiat-pegged mechanism to ensure the price stability of TBD token. The method is transparent throughout the token's lifecycle. The token contract, token supply and fiat reserve are auditable by all.
            </p>
        </div>
        <div class="d-block d-sm-block d-md-none  col-sm-12 ui vertical segment" style="padding-bottom: 30px;"></div>
        <div class="d-block d-sm-block d-md-none  col-sm-12 " style="height: 30px;"></div>
        <div class="col-md-4 col-sm-12" style="text-align: center; ">
            <img src="data:image/svg+xml;base64,PHN2ZyBpZD0iQ2FwYV8xIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1MTIgNTEyIiBoZWlnaHQ9IjUxMiIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHdpZHRoPSI1MTIiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0ibTUwNS41MDYgMjY0Ljg2Yy00LjExMS0uNTYyLTcuODk1IDIuMzE2LTguNDU2IDYuNDI1LTMuNjI0IDI2LjU0LTE2Ljc1OCA1MC45NTQtMzYuOTg0IDY4Ljc0My0yMC40MDggMTcuOTQ5LTQ2LjYzMSAyNy44MzQtNzMuODQyIDI3LjgzNC0xNC44MDUgMC0yOS4xODUtMi44NDQtNDIuNzQxLTguNDU0LS4yMjUtLjA5My0uNDg1LS4xODQtLjcxOS0uMjU1LS4xNTktLjA0OC0xNi4xNTYtNS4wNDUtMzkuOTc1LTIzLjk1My0zLjI0OS0yLjU3OS03Ljk3Mi0yLjAzNS0xMC41NTEgMS4yMTMtMi41NzggMy4yNDktMi4wMzUgNy45NzIgMS4yMTMgMTAuNTUxIDI0LjQzNiAxOS4zOTYgNDEuNjA0IDI1LjQ4MiA0NC41OTcgMjYuNDQ5IDE1LjI5NSA2LjI4MiAzMS41MDEgOS40NjggNDguMTc2IDkuNDY4IDMwLjg2MyAwIDYwLjYxLTExLjIxNCA4My43NjEtMzEuNTc1IDIyLjkzNy0yMC4xNzMgMzcuODMzLTQ3Ljg3IDQxLjk0Ni03Ny45ODguNTYtNC4xMTEtMi4zMTYtNy44OTctNi40MjUtOC40NTh6Ii8+PHBhdGggZD0ibTMyNC43NzEgMzA3Ljk4YzE1LjkzOCAxNC4yMDggMzUuNzYgMjguNzMyIDYxLjk1NiAyOC43MzFoLjAyNmM0NC40ODctLjAxNyA4MC42NzktMzYuMjIzIDgwLjY3OS04MC43MTEgMC00NC41MDQtMzYuMjA2LTgwLjcxLTgwLjcxLTgwLjcxLS4wMDIgMCAwIDAtLjAwMSAwLTMwLjY4NCAwLTUxLjYwMiAxNy44MTgtNjYuMzk1IDMzLjM0LTIxLjY0NiAyMi43MTQtNzAuNzM2IDgwLjk1Ny03Mi41MiA4My4yMTQtNDcuMDY1IDU5LjU2Ny03NS4wMTggNjYuNzQtNzYuNTQ4IDY3LjA5MS0uNjM4LjA4My0xLjAyNy4yMTgtMS42MzcuNDcxLTEyLjA5MiA1LjAwNC0yNC44OTYgNy44MTYtMzguMDU0IDguMzU3bC00Ljc2My4wOTZjLTMwLjAwNC0uMDItNTguMTY5LTExLjc1Ny03OS4zMTEtMzMuMDU0LTIxLjE1OC0yMS4zMTMtMzIuNjktNDkuNTk3LTMyLjQ3LTc5LjY0My4yMTctMjkuNzkxIDEyLjEyNy01Ny43ODYgMzMuNTM1LTc4LjgyNyAyMS4xNTItMjAuNzkgNDguOTI3LTMyLjE5NyA3OC4zMjktMzIuMTk3LjM3NiAwIC43NTYuMDAyIDEuMTMyLjAwNSAxMy45MTQuMTM5IDI3LjQ2NSAyLjggNDAuMjgyIDcuOTEyLjc5MS4zMTUgMS41ODIuNjI1IDIuMzguOTI0IDMuOTA5IDEuNDYyIDM4LjM2NyAxNS4zOCA3Ny4xNTQgNjQuNjE2IDAgMC0xNi42NjUgMjAuMzUzLTIwLjUxOSAyNC45MjktNy41NjEtOC4yMzctMjEuODA3LTIzLjE5NC0zOC45ODItMzguNTA1LTE1LjkzOC0xNC4yMDgtMzUuNzU5LTI4LjczMS02MS45NTQtMjguNzMxLS4wMDggMC0uMDIgMC0uMDI3IDAtMi42NzggMC01LjM4My4xMzQtOC4wMzguMzk4LTQuMTI3LjQxMS03LjE0IDQuMDg4LTYuNzMgOC4yMTVzNC4wOTYgNy4xMzYgOC4yMTUgNi43M2MyLjE2NS0uMjE1IDQuMzctLjMyNSA2LjU1Ny0uMzI1aC4wMjhjMTEuODcyIDAgMjcuMzMgMi45NyA1MS45NTUgMjQuOTIyIDE3LjUwNiAxNS42MDcgMzIuMTc0IDMxLjE3MiAzOS4xMzIgMzguNzk3LTEwLjk5OSAxMi42NTMtMjIuNzc1IDI1LjU1OS0zNS41NjYgMzguOTgtMTguOTA1IDE5LjgzNy0zNi4wMjkgMjguNjg0LTU1LjUyNCAyOC42ODQtMzYuMjIyIDAtNjUuNjkyLTI5LjQ3LTY1LjY5Mi02NS42OTIgMC0yMi4wMTkgMTAuOTU0LTQyLjQ2OCAyOS4zMDItNTQuNzAyIDMuNDUxLTIuMzAxIDQuMzgzLTYuOTYzIDIuMDgyLTEwLjQxNC0yLjMtMy40NS02Ljk2Mi00LjM4Mi0xMC40MTQtMi4wODItMjIuNTM1IDE1LjAyOS0zNS45ODkgNDAuMTUtMzUuOTg5IDY3LjIwMSAwIDQ0LjUwMyAzNi4yMDYgODAuNzEgODAuNzEgODAuNzExIDMwLjY4NyAwIDUxLjYwMy0xNy44MTggNjYuMzk2LTMzLjM0MSAyMS42NDYtMjIuNzE0IDczLjg1Ni04NC45MDEgNzMuODU2LTg0LjkwMS4wMzItLjAzOC4wNjMtLjA3Ny4wOTQtLjExNSA0Ni4yMDMtNTcuOTE5IDczLjYwNy02NC45NDMgNzUuMTE4LTY1LjI5LjYzOC0uMDgyIDEuMDI2LS4yMTggMS42MzctLjQ3MSAxMi4wOTItNS4wMDQgMjQuODk1LTcuODE2IDM4LjA1NC04LjM1OGw0Ljc2MS0uMDk2YzI2Ljc5OS4wMTggNTIuNzEyIDkuNjU1IDcyLjk3MyAyNy4xNDEgMjAuMDc0IDE3LjMyNCAzMy4zNzcgNDEuMjA3IDM3LjQ1OSA2Ny4yNS42NDIgNC4wOTcgNC40ODQgNi44OTUgOC41ODIgNi4yNTYgNC4wOTctLjY0MyA2Ljg5OC00LjQ4NSA2LjI1Ni04LjU4Mi00LjYzMy0yOS41NTMtMTkuNzItNTYuNjQ3LTQyLjQ4NC03Ni4yOTMtMjMuMDA2LTE5Ljg1NS01Mi40MzItMzAuNzktODIuODU5LTMwLjc5LS4wNSAwLS4xMDEgMC0uMTUyLjAwMWwtNC45MTcuMDk5Yy0uMDUxLjAwMS0uMTAyLjAwMi0uMTUzLjAwNS0xNC43MDYuNTk2LTI5LjAyNSAzLjY5My00Mi41NzQgOS4yMDctNi4yODMgMS40LTM0LjgxNCAxMC42MDUtODAuODMgNjcuMzExLTM5LjU4NS00OS4yNzctNzQuNzQxLTY0LjI0NC04MS42NTgtNjYuODMtLjY5OC0uMjYxLTEuMzkxLS41MzMtMi4wOC0uODA4LTE0LjU0NC01LjgwMS0yOS45MTctOC44MjItNDUuNjkzLTguOTc5LTMzLjg2My0uMzU0LTY1Ljg0OSAxMi42MjUtOTAuMTM4IDM2LjQ5OC0yNC4yNzQgMjMuODYtMzcuNzc5IDU1LjYyLTM4LjAyNiA4OS40My0uMjQ4IDM0LjA3OCAxMi44MzEgNjYuMTU4IDM2LjgzIDkwLjMzMyAyNC4wMDEgMjQuMTc3IDU1Ljk4IDM3LjQ5MiA5MC4wNDYgMzcuNDkyLjA1IDAgLjEwMSAwIC4xNTItLjAwMWw0LjkxNy0uMDk5Yy4wNTEtLjAwMS4xMDItLjAwMi4xNTMtLjAwNSAxNC43MDUtLjU5NiAyOS4wMjUtMy42OTMgNDIuNTczLTkuMjA3IDYuMjA3LTEuMzgxIDM0LjA0OS0xMC4zNTggNzguOTYzLTY1LjAyNCA0LjI5MiA0LjkwNCA4LjY0NiA5LjYyNSAxMy4wMDIgMTQuMDg0IDIuODk4IDIuOTY2IDcuNjUyIDMuMDIyIDEwLjYxOS4xMjUgMi45NjctMi44OTggMy4wMjMtNy42NTIuMTI1LTEwLjYxOS00LjcyOC00Ljg0LTkuNDYxLTEwLjAwNC0xNC4xMS0xNS4zOTEgMy45NDQtNC43NTkgMTguNjU5LTIyLjY5MyAyMi41MTUtMjcuMjcxIDcuNTU4IDguMjM1IDIxLjgwOSAyMy4xOTcgMzguOTgzIDM4LjUwOHptNi40MjctODguOTg4YzE4LjkwNS0xOS44MzggMzYuMDI5LTI4LjY4NCA1NS41MjQtMjguNjgzIDM2LjIyMiAwIDY1LjY5MiAyOS40NjkgNjUuNjkyIDY1LjY5MiAwIDM2LjIwOS0yOS40NTggNjUuNjc4LTY1LjY2NyA2NS42OTItLjAwOSAwLS4wMTcgMC0uMDI2IDAtMTEuODcyIDAtMjcuMzMyLTIuOTctNTEuOTU3LTI0LjkyMy0xNy41MDktMTUuNjEtMzIuMTc4LTMxLjE3OC0zOS4xMzMtMzguODAyIDEwLjk5OC0xMi42NSAyMi43NzgtMjUuNTU3IDM1LjU2Ny0zOC45NzZ6Ii8+PC9zdmc+"
                 class="ui tiny centered rounded image">
            <h3 style="font-size: 2em"
                class="ui header"
            >Unlimited possibilities</h3>
            <p style="font-size: 1.2em">
                TBD enables wide ranges of use cases, many more than what most cryptocurrencies with high volatility can offer. As digital economy is rising, more exchanges, wallets, applications, service providers from all over the world will be utilizing this. TBD will be a vital component to complete digital ecosystem, especially in Asia market.
            </p>
        </div>
    </div>
</div>
<div class="container" >
    <div class="row" style="margin: 0px; padding-top: 50px;">

    </div>
</div>


<!-- ช่วงที่ 3 ของหน้า -->
<div class="ui vertical segment">
    <div class="ui stackable internally celled equal width grid">
        <div class="center aligned row" style="width: 100%!important;margin-right: -15px;margin-left: -15px">

        </div>
    </div>
</div>
<!-- ส่วนที่4 -->
<div class="container-fluid" style="padding-bottom: 30px; background-color: black">
    <div class="container">
        <div class="row" style="padding-left: 10vw; padding-top: 8em; padding-bottom: 8em;" >
            <div class="col-sm-12 col-md-6" >
                <div style="font-size:2em;color: white;text-align: left; padding-bottom: 10px;" class="ui inverted header">
                    Introduction to relevance coin
                </div>
                <div style="font-size:1.33em; color: rgba(255,255,255,.9); padding-bottom: 10px; min-height: 3.5em;">
                    Introduction to TBD - Thai Baht Digital Token <br>

                </div>
                <a href="{{url('TBD-Whitepaper-[v.3.2].pdf')}}" class="ui large button" role="button">
                    Read Our Whitepaper
                </a>
            </div>
            <div class="d-block d-sm-block d-md-none  col-sm-12 ui vertical segment" style="padding-bottom: 30px;"></div>
            <div class="d-block d-sm-block d-md-none  col-sm-12 " style="height: 30px;"></div>
            <div class="col-sm-12 col-md-6" >
                <div style="font-size:2em;color: white;text-align: left; padding-bottom: 10px;" class="ui inverted header">
                    Balance Checking
                </div>
                <div style="font-size:1.33em; color: rgba(255,255,255,.9); line-height: 1.4em; padding-bottom: 10px; min-height: 3.5em;">
                    Deposit Account 1 THB <br>
                    Circulate Token 1 TBD
                </div>
                <a href="https://etherscan.io/address/0x39db4d160a7f3c5f772e6d816d6899cdadeb3bd4" class="ui large button" role="button">
                    Check on Etherscan
                </a>
            </div>
        </div>
    </div>
</div>

<div class="ui vertical segment">
</div>
<!-- ส่วนที่5 -->
<div class="container" style="padding-bottom: 30px">
    <div class="row" style="margin: 0px; padding-top: 50px;">
        <div class="col-md-12 col-sm-12" style="padding-bottom: 30px;">
            <h3 style="font-size: 2em;text-align: center" class="ui header">
                Who is this for?
            </h3>
        </div>
        <div class="col-md-6 col-sm-12 ui " style="text-align: center;">
            <img src="{{url("img/in.svg")}}" class="ui tiny centered rounded image">
            <h3 style="font-size: 2em" class="ui header">Investors</h3>
            <p style="font-size: 1.28em">TBD can be used as a self-sovereign store of value equivalent to fiat, by storing it on their own wallet, with a private key, off the exchange. This helps avoid the risk of storing tokens on an exchange.</p>
        </div>
        <div class="d-block d-sm-block d-md-none  col-sm-12 ui vertical segment" style="padding-bottom: 30px;"></div>
        <div class="d-block d-sm-block d-md-none  col-sm-12 " style="height: 30px;"></div>
        <div class="col-md-6 col-sm-12 " style="text-align: center;">
            <img src="{{url("img/ex.svg")}}" class="ui tiny centered rounded image">
            <h3 style="font-size: 2em" class="ui header">Exchanges</h3>
            <p style="font-size: 1.28em">TBD can be used as a Thai baht pair for trading cryptocurrencies and securities on Thai exchanges as well as international ones</p>
        </div>
        <div class="d-block d-sm-block d-md-none  col-sm-12 ui vertical segment" style="padding-bottom: 30px;"></div>
        <div class="d-block d-sm-block d-md-none  col-sm-12 " style="height: 30px;"></div>
        <div class="col-md-6 col-sm-12 " style="text-align: center;">
            <img src="{{url("img/dev.svg")}}" class="ui tiny centered rounded image">
            <h3 style="font-size: 2em" class="ui header">Developers</h3>
            <p style="font-size: 1.28em">TBD can be utilized as a native currency or alternative currency in various applications, especially decentralized ones that lack Thai baht or relevance token.</p>
        </div>
        <div class="d-block d-sm-block d-md-none  col-sm-12 ui vertical segment" style="padding-bottom: 30px;"></div>
        <div class="d-block d-sm-block d-md-none  col-sm-12 " style="height: 30px;"></div>
        <div class="col-md-6 col-sm-12 ui" style="text-align: center;">
            <img src="{{url("img/reg.svg")}}" class="ui tiny centered rounded image">
            <h3 style="font-size: 2em" class="ui header">Regulators</h3>
            <p style="font-size: 1.28em">TBD offers a better transparency and auditability for regulators. For instance, TBD token’s activity can be tracked and its holders can be mostly identified, for instance, the amount of TBD token held by retail investors, local exchanges, foreign exchanges and etc. The regulator may request such information in circumstances such as money laundering, auditing, exchange reserve threshold checking and etc.</p>
        </div>
    </div>
</div>
<div class="container-fluid" style="padding-bottom: 30px; background-color: black; padding-left: 50px">
    <div class="container" style="padding-bottom: 30px">
        <div class="row" style="margin: 0px; padding-top: 50px;">
            <div style="align-items: stretch;width: 100%!important;padding: 0" class="row">
                <div class="five wide column">
                    <h4 class="ui inverted header" style="color: white;text-align: left; ">About</h4>
                    <div role="list" class="ui inverted link list item">
                        <a style="color: rgba(255,255,255,.5)!important;" href="{{url('contact')}}">Contact Us</a>
                    </div>
                </div>
                <div class="five wide column">
                    <h4 class="ui inverted header" style="color: white;text-align: left; ">Materials</h4>
                    <div role="list" class="ui inverted link list item">
                        <a href="{{url('TBD-Whitepaper-[v.3.2].pdf')}}"
                           style="color: rgba(255,255,255,.5)!important;"
                        >Whitepaper</a>

                    </div>
                    <div role="list" class="ui inverted link list item">

                        <a href="{{url('TBD-AML-Policy.pdf')}}"
                           style="color: rgba(255,255,255,.5)!important;"
                        >AML Policy</a>
                    </div>
                </div>
                <div class="five wide column" >
                    <h4 class="ui inverted header" style="color: white;text-align: left">
                        Thai Baht Digital </h4>
                    <div role="list" class="ui inverted link list item" style="text-align: left">
                        <img  width="30px" src="{{url("img/TBD_Logo.png")}}" >
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


{{--<!-- footer -->--}}
{{--<div style="padding:5em 0" class="ui inverted vertical segment">--}}
{{--    <div class="ui container">--}}
{{--        <div class="ui inverted stackable divided grid">--}}
{{--           --}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
