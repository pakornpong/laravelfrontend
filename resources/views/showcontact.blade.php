<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57"          href="{{url('fav/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60"          href="{{url('fav/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72"          href="{{url('fav/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76"          href="{{url('fav/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114"        href="{{url('fav/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120"        href="{{url('fav/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144"        href="{{url('fav/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152"        href="{{url('fav/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180"        href="{{url('fav/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"   href="{{url('fav/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32"     href="{{url('fav/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96"     href="{{url('fav/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16"     href="{{url('fav/favicon-16x16.png')}}">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <title>Thai Baht Digital</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">


    <style>
        tr{
            height: 50px;
        }

        .form-control{
            width: 100%
        }
        .nav-link{
            color:#111111;
        }

        #all{
            height: calc( 100vh);
            width: 100vw;
            margin-top: 25px;
            font-size: 21px;
            text-align: center;
            animation: fadein 2s;
            -moz-animation: fadein 2s; /* Firefox */
            -webkit-animation: fadein 2s; /* Safari and Chrome */
            -o-animation: fadein 2s; /* Opera */
        }
        @keyframes fadein {
            from {
                background-color: gray;
            }
            to {
                background-color: white;
            }
        }
        @-moz-keyframes fadein { /* Firefox */
            from {
                background-color: gray;
            }
            to {
                background-color: white;
            }
        }
        @-webkit-keyframes fadein { /* Safari and Chrome */
            from {
                background-color: gray;
            }
            to {
                background-color: white;
            }
        }
        @-o-keyframes fadein { /* Opera */
            from {
                background-color: gray;
            }
            to {
                background-color: white;
            }
        }
    </style>


</head>
<body>
<div class="container-fluid" id="all" style="margin-top: 0px;">
    <div class="sticky"  style="z-index: 999;">
        <nav class="navbar navbar-expand-sm">
            <a class="nav-link" href="{{url('')}}">
                <span>Home</span>
            </a>
        </nav>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="row" style="height: 20px;"></div>
            <div class="row" style="text-align: center">
                <div class="col-12" style="padding-bottom: 0px">

                </div>
            </div>
            <div class="row" >
                <div class="col-sm-12 " style="text-align: center" >
                    <img style="height:80px; width: 80px;" src="{{url('img/TBD_Logo.png')}}">
                    <h5 class="card-title" style="padding-top: 15px;color: green">Thank you {{$contact['name']}} for contacting us! </h5>
                </div>

                <div class="d-none d-sm-block col-sm-3 " >
                </div>
                <div class="col-sm-12 col-md-6" >
                    <div class="card-body">

                            <div class="form-inline">
                                <label for="Email" class="col-sm-6 control-label">Email address:</label>
                                <h4 style="margin-top: 4px;" class="form-control col-sm-6" id="Email">{{$contact['email_address']}}</h4>
                            </div>
                            <div class="form-inline">
                                <label for="First_name"  class="col-sm-6 control-label">Name:</label>
                                <h4 style="margin-top: 4px;" class="form-control col-sm-6" id="First_name">{{$contact['name']}}</h4>
                            </div>
                            <div class="form-inline">
                                <label for="Last_name"  class="col-sm-6 control-label">Message:</label>
                                <h4 style="margin-top: 4px;" class="form-control col-sm-6" id="Last_name">{{$contact['message']}}</h4>
                            </div>


                    </div>
                </div>

            </div>


        </div>

    </div>
</div>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>

