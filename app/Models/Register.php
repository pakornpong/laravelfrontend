<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    protected $table = 'registers';
    protected $primaryKey = 'id';
    protected $fillable = [
        'email_address',
        'password',
        'password_confirmation',
        'firstname',
        'lastname',
        'phonenumber',
        'province',
        'country'
    ];
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }
    public function setPasswordconfirmationAttribute($password_confirmation)
    {
        $this->attributes['password_confirmation'] = bcrypt($password_confirmation);
    }
}
