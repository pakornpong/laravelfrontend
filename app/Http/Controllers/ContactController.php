<?php

namespace App\Http\Controllers;
use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index()
    {
        return view('/contact');
    }

    public function create()
    {
        return view('/contact');
    }

    public function store(Request $request)
    {
        $captcha = "";
        if(isset($_POST['g-recaptcha-response'])){
            $captcha=$_POST['g-recaptcha-response'];
        }

        if (!self::checkGoogleCaptcha($captcha)){
            return back()->withErrors(['Please complete the Captcha']);
        }


        $request->validate([
            'email_address' => 'required',
            'name' => 'required',
            'message' => 'required',
        ]);
        $contacts = new Contact([
            'email_address' => $request->get('email_address'),
            'name' => $request->get('name'),
            'message' => $request->get('message'),
        ]);
        $contacts->save();

        $response = array();
        $response['contact'] = $contacts;

        return view('showcontact')->with($response);
    }
}
