<?php

namespace App\Http\Controllers;
use App\Models\Register;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    public function index()
    {
        return view('/register');
    }
    public function create()
    {
        return view('/register');
    }
    public function store(Request $request)
    {
        $captcha = "";
        if(isset($_POST['g-recaptcha-response'])){
            $captcha=$_POST['g-recaptcha-response'];
        }

        if (!self::checkGoogleCaptcha($captcha)){
           return back()->withErrors(['Please complete the Captcha']);
        }

        $request->validate([
             'email_address'=> 'required|unique:registers',
             'password'=> 'required|min:6|confirmed',
//             'confirm_password'=> 'required',
             'firstname'=> 'required',
             'lastname'=> 'required',
             'phonenumber'=> 'required|max:10',
             'province'=> 'required',
             'country'=> 'required',
        ]);
        $registers = new Register([
             'email_address'=> $request->get( 'email_address'),
             'password'=> $request->get( 'password'),
             'password_confirmation'=> $request->get( 'password_confirmation'),
             'firstname'=> $request->get( 'firstname'),
             'lastname'=> $request->get( 'lastname'),
             'phonenumber'=> $request->get( 'phonenumber'),
             'province'=> $request->get( 'province'),
             'country'=> $request->get( 'country'),
        ]);
        $registers->save();

        $response = array();
        $response['registers'] = $registers;


        return view('show')->with($response);
    }

}
